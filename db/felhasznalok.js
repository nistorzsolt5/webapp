import dbConnection from './connection.js';

export function insertFelhasznalo(felhasznalonev, password) {
  return dbConnection.executeQuery(
    `insert into felhasznalo(Nev, Password, Role)
     values (?, ?, ?)`,
    [felhasznalonev, password, 'user'],
  );
}

export function findAllFelhasznalo() {
  return dbConnection.executeQuery('select * from felhasznalo');
}

export function findUserCredentials(name) {
  return dbConnection.executeQuery('select * from felhasznalo where Nev = ?', name);
}
export function findUserCredentialID(name) {
  return dbConnection.executeQuery('select FID from felhasznalo where Nev = ?', name);
}
