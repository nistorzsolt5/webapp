//
// Itt csak a foglalasokkal vannak fuggvenyek
//
import dbConnection from './connection.js';

export function findFoglalasok(id) {
  return dbConnection.executeQuery(`
    select * from foglalas as f 
      join vendeglo as v on f.VID = v.VID
      join felhasznalo as fe on fe.FID = f.FID
      where v.VID = ? and status like 'accepted'`, id);
}

export function findAllFoglalas() {
  return dbConnection.executeQuery('select * from foglalas where status like \'accepted\'');
}

export function findAllFoglalasUser(FID) {
  return dbConnection.executeQuery(`
  select * from foglalas as f
    join felhasznalo as fe on fe.FID = f.FID
    join vendeglo as v on v.VID = f.VID
  where fe.FID = ?
  order by VNev, Ora`, FID);
}

export function findAllFoglalasAdmin() {
  return dbConnection.executeQuery(`
  select * from foglalas as f
    join felhasznalo as fe on fe.FID = f.FID
    join vendeglo as v on v.VID = f.VID
  order by VNev, Ora`);
}

export function findFoglalasFiltered(id, name, status, idopont) {
  let querry = `
  select * from foglalas as f
    join felhasznalo as fe on fe.FID = f.FID
    join vendeglo as v on v.VID = f.VID
    where VNev LIKE '%`;

  if (name) {
    querry += ('?', name);
  }
  querry += '%\' ';

  if (id !== 5) {
    querry += ' and fe.FID = ';
    querry += ('?', id);
  }

  if (status) {
    querry += ' and status LIKE \'%';
    querry += ('?', status);
    querry += '%\' ';
  }
  if (idopont) {
    querry += ' and Ora = ';
    querry += ('?', idopont);
  }

  querry += ' order by VNev, Ora';

  console.log(querry);

  return dbConnection.executeQuery(querry);
}

export function findFoglalasAsztalok(vid, idopont) {
  let querry = `
  select AID from foglalas as f
    join felhasznalo as fe on fe.FID = f.FID
    join vendeglo as v on v.VID = f.VID
    where v.VID = `;

  querry += ('?', vid);

  if (idopont) {
    querry += ' and Ora = ';
    querry += ('?', idopont);
  }

  console.log(querry);

  return dbConnection.executeQuery(querry);
}

export function insertFoglalas(aid, vid, fid, ora) {
  return dbConnection.executeQuery(
    `insert into foglalas
     values (default, ?, ?, ?, ?, ?)`,
    [vid, fid, ora, 'pending', aid],
  );
}

export function deleteFoglalas(id) {
  return dbConnection.executeQuery(`
   delete from foglalas 
    where FoglalasId = ?`, id);
}

export function acceptFoglalas(id) {
  return dbConnection.executeQuery(`
  update  foglalas set status = 'accepted'
  where FoglalasId = ?`, id);
}

export function rejectFoglalas(id) {
  return dbConnection.executeQuery(`
  update  foglalas set status = 'rejected'
  where FoglalasId = ?`, id);
}
