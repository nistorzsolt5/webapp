import dbConnection from './connection.js';

export function getIdopontok(id) {
  return dbConnection.executeQuery('select Nyitas,Zaras from vendeglo where VID = ?', id);
}

export function insertVendeglo(vendeglo) {
  return dbConnection.executeQuery(
    `insert into vendeglo
     values (default, ?, ?, ?, ?, ?, ?, ?)`,
    [vendeglo.form_name,
      vendeglo.form_varos,
      vendeglo.form_utca,
      vendeglo.form_szam,
      vendeglo.form_telefon,
      vendeglo.form_nyitas,
      vendeglo.form_zaras],
  );
  // const [elsoElemVisszaterites] =
  // return elsoElemVisszaterites;
}
export function findAllVendeglo() {
  return dbConnection.executeQuery('select * from vendeglo');
}
export function findVendeglo(id) {
  return dbConnection.executeQuery('select * from vendeglo where VID = ?', id);
}
export function findVendegloByName(name, helyseg, nyitas, zaras) {
  let querry = 'select * from vendeglo where VNev LIKE \'%';
  if (name) {
    querry += ('?', name);
  }
  querry += '%\'';
  if (helyseg) {
    querry += ' and Varos LIKE \'%';
    querry += ('?', helyseg);
    querry += '%\'';
  }
  if (nyitas) {
    querry += ' and Nyitas >= ';
    querry += ('?', nyitas);
  }
  if (zaras) {
    querry += ' and Zaras <= ';
    querry += ('?', zaras);
  }
  console.log(querry);

  return dbConnection.executeQuery(querry);
}
export const findAllV = () => {
  const executeQuery = 'SELECT * FROM vendeglo';
  return dbConnection.executeexecuteQuery(executeQuery);
};

export function insertKep(id, path, name) {
  return dbConnection.executeQuery(
    `insert into kepek
     values (default, ?, ?, ?)`,
    [id, path, name],
  );
}
export function findKep(id) {
  return dbConnection.executeQuery('select * from kepek where VID = ?', id);
}
