import mysql2 from 'mysql2';
import autoBind from 'auto-bind';

export class DbConnection {
  constructor() {
    // Létrehozunk egy connection poolt
    this.pool = mysql2.createPool({
      host: 'localhost',
      port: 3306,
      user: 'root',
      password: 'root',
      database: 'vendeglodb',
      connectionLimit: 5,
    });

    autoBind(this);
  }

  executeQuery(query, options = []) {
    return new Promise((resolve, reject) => {
      this.pool.query(query, options, (error, results) => {
        if (error) {
          reject(new Error(`Error while running '${query}: ${error}'`));
        }
        resolve(results);
      });
    });
  }
}

export default new DbConnection();
