import express from 'express';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import { checkJWT, validateJWT } from './middleware/jwt_middleware.js';
import requestRoutes from './routes/vendeglok_get_post.js';
import authRoutes from './routes/auth.js';

const app = express();
app.use(morgan('tiny'));
app.use(express.static('static/'));
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.set('view engine', 'ejs');
app.use('/', checkJWT);
app.use('/', validateJWT);
app.use('/', authRoutes);
app.use('/', requestRoutes);

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
