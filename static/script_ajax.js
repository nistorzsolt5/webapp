// eslint-disable-next-line
function getMoreInfoVendeglo(VendegloID) {
  try {
    fetch(`/getMoreInfoVendeglo/${VendegloID}`)
      .then((response) => response.text())
      .then((message) => {
        console.log(message);

        const s = JSON.parse(message);
        const szoveg = `${s.Varos},  ${s.Utca},  ${s.Szam}\n${s.Telefon}`;

        document.getElementById(`getMoreInfoVendeglo/${VendegloID}`).innerText = szoveg;
      });
  } catch (err) {
    console.log(err);
  }
}

// eslint-disable-next-line
function showPassword(melyik) {
  const x = document.getElementById(melyik);
  if (x.type === 'password') {
    x.type = 'text';
  } else {
    x.type = 'password';
  }
}

// eslint-disable-next-line
async function szabadAsztalok() {
  console.log(document.getElementById('vendeg').value);
  const idopont = document.getElementById('form_kliens_idopont').value;
  document.getElementById('submitgomb').disabled = true;

  if (idopont) {
    try {
      const nyitva = await fetch(`/nyitvaVendeglo/${document.getElementById('form_kliens_idopont').value}/${document.getElementById('vendeg').value}`, {
        method: 'GET',
      });

      const valasz = await nyitva.json();
      // ha nincs nyitva nem lehet foglalast csinalni
      if (valasz === 'false') {
        document.getElementById('szoveg').innerHTML = 'ZARVA VAN AZ ETTEREM EBBEN AZ ORABAN';

        for (let i = 1; i < 10; i += 1) {
          const text = `radio${i}`;
          document.getElementById(`${text}`).disabled = true;
        }
      // mas esetben igen
      } else {
        document.getElementById('szoveg').innerHTML = 'Asztalok: ';
        for (let i = 1; i < 10; i += 1) {
          const text = `radio${i}`;
          document.getElementById(`${text}`).disabled = false;
          document.getElementById('submitgomb').disabled = false;
        }
        const f = await fetch(`/foglaltAsztal/${document.getElementById('form_kliens_idopont').value}/${document.getElementById('vendeg').value}`, {
          method: 'GET',
        });

        const s = await f.json();

        // ha vannak foglalasok mar akkore azokra nem engedek tobbet rakni
        if (s.length) {
          for (let i = 0; i < s.length; i += 1) {
            const text = `radio${s[i].AID}`;
            document.getElementById(`${text}`).disabled = true;
          }
        }
      }
    } catch (err) {
      console.log(err);
    }
  } else {
    for (let i = 1; i < 10; i += 1) {
      const text = `radio${i}`;
      document.getElementById(`${text}`).disabled = true;
    }
  }
}

// eslint-disable-next-line
async function deleteFoglalas(FoglalasID) {
  // eslint-disable-next-line
  if (confirm('Are you sure?')) {
    try {
      // document.getElementById(`tr/${FoglalasID}`).remove();
      const result = await fetch(`/deleteFoglalas/${FoglalasID}`, {
        method: 'DELETE',
      });
      console.log('TORLES STATUS: ', result.status);
      if (result.status === 204) {
        document.getElementById(`tr/${FoglalasID}`).remove();
        document.getElementById('alertt').innerHTML = 'Succesfully deleted';
        document.getElementById('alert_fo').style.display = 'block';
      }
    } catch (err) {
      console.log(err);
      document.getElementById('alertt').innerHTML = 'NEM SIKERULT A TORLES';
      document.getElementById('alert_fo').style.display = 'block';
    }
  }
}

// eslint-disable-next-line
async function acceptFoglalas(FoglalasID) {
  // eslint-disable-next-line
  if (confirm('Are you sure?')) {
    try {
      // document.getElementById(`tr/${FoglalasID}`).remove();
      const result = await fetch(`/acceptFoglalas/${FoglalasID}`, {
        method: 'PATCH',
      });
      console.log('UPDATE STATUS: ', result.status);
      if (result.status === 204) {
        // document.getElementById(`tr/${FoglalasID}`).update('accepted');
        document.getElementById(`td/status/${FoglalasID}`).innerHTML = 'accepted';
        document.getElementById('alertt').innerHTML = 'Succesfully accepted';
        document.getElementById('alert_fo').style.display = 'block';
      }
    } catch (err) {
      console.log(err);
      document.getElementById('alertt').innerHTML = 'NEM SIKERULT ACCEPTELNI';
      document.getElementById('alert_fo').style.display = 'block';
    }
  }
}

// eslint-disable-next-line
async function rejectFoglalas(FoglalasID) {
  // eslint-disable-next-line
  if (confirm('Are you sure?')) {
    try {
      const result = await fetch(`/rejectFoglalas/${FoglalasID}`, {
        method: 'PATCH',
      });
      console.log('UPDATE STATUS: ', result.status);
      if (result.status === 204) {
        document.getElementById(`td/status/${FoglalasID}`).innerHTML = 'rejected';
        document.getElementById('alertt').innerHTML = 'Succesfully rejected';
        document.getElementById('alert_fo').style.display = 'block';
        // alert('Sikeresen elutasitottuk a foglalast');
      }
    } catch (err) {
      console.log(err);
      document.getElementById('alertt').innerHTML = 'NEM SIKERULT REJECTELNI';
      document.getElementById('alert_fo').style.display = 'block';
    }
  }
}
