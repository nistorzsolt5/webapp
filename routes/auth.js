import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { secret } from '../secret.js';
import { findUserCredentials, insertFelhasznalo } from '../db/felhasznalok.js';

const router = express.Router();

//
// Login betoltese
//
router.get('/loginoldal', (req, res) => {
  try {
    // const vendeglok = await findAllVendeglo();
    // console.log(vendeglok);
    res.render('login');
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'NEM SIKERULT BETOLTENI A LOGIN OLDALT' });
  }
});

//
// Form feldolgozasa login
//
router.post('/form_login', async (req, res) => {
  const credentials = await findUserCredentials(req.body.username);
  // ha nem letezik
  if (credentials[0]) {
    if (req.body.username === credentials[0].Nev
      && await bcrypt.compare(req.body.password, credentials[0].Password)) {
      const token = jwt.sign({
        name: credentials[0].Nev,
        id: credentials[0].FID,
        role: credentials[0].Role,
      }, secret);

      res.cookie('auth', token, { httpOnly: true, sameSite: 'strict' });
      res.type('.html').status(200).render('succes', { message: `Welcome ${credentials[0].Nev}` });
    } else {
      res.type('.html').status(401).render('error', { message: 'NEM jelentkeztel be | SOMETHINGS WRONG' });
    }
  } else {
    res.type('.html').status(401).render('error', { message: 'NEM LETEZIK EZ A USERNAME ES PASSWORD KOMBINACIO' });
  }
});

//
// Regisztracio
//
router.post('/form_registration', async (req, res) => {
  const credentials = await findUserCredentials(req.body.username_registration);

  if (credentials[0]) {
    res.type('.html').status(401).render('error', { message: 'MAR LETEZIK A FELHASZNALO' });
  } else {
    try {
      const username = req.body.username_registration;
      const password = bcrypt.hashSync(req.body.password_registration, 10);
      await insertFelhasznalo(username, password);

      res.type('.html').status(200).render('succes', { message: 'Sikeresen Regisztralt, most bejelentkezhet' });
    } catch (err) {
      res.type('.html').status(404).render('error', { message: `NEM jelentkeztel be | SOMETHINGS WRONG ${err.message}` });
    }
  }
});

router.get('/logout', async (req, res) => {
  res.clearCookie('auth');
  return res.redirect('/');
});

export default router;
