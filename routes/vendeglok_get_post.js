import express from 'express';
import * as p from 'path';
import eformidable from 'express-formidable';
import jwt from 'jsonwebtoken';
import * as valid from '../middleware/validate.js';
import {
  findAllVendeglo,
  insertVendeglo,
  findVendeglo,
  insertKep,
  getIdopontok,
  findKep,
  findVendegloByName,
} from '../db/vendeglodb.js';
import { secret } from '../secret.js';

// import { findAllFelhasznalo } from '../db/felhasznalok.js';

import {
  insertFoglalas,
  findFoglalasok,
  findAllFoglalasAdmin,
  deleteFoglalas,
  acceptFoglalas,
  rejectFoglalas,
  findAllFoglalasUser,
  findFoglalasFiltered,
  findFoglalasAsztalok,
} from '../db/foglalasok.js';

// import {
//   findUserCredentials,
// } from '../db/felhasznalok.js';

const router = express.Router();
const regex = /^[0-9]*/;

//
// Fooldal betoltese
//
router.get('/', async (req, res) => {
  if (req.cookies.auth) {
    console.log(
      await jwt.verify(req.cookies.auth, secret).name,
      await jwt.verify(req.cookies.auth, secret).id,
    );
  }
  try {
    const vendeglok = await findAllVendeglo();
    res.render('index', { vendeglok });
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'NEM SIKERULT BETOLTENI A VENDEGLOKET' });
  }
});

//
// Vendeglo beszurasa
//
router.post('/vendegloBeszuras', async (req, res) => {
  valid.checkNotEmpty(req.body).catch(() => {
    res.type('.html').status(400).render('error', { message: 'URES MEZOT KAPTUNK!' });
    console.log('EMPTY FIELD error');
  });

  valid.checkFields(req.body).catch(() => {
    res.type('.html').status(400).render('error', { message: 'error WITH THE DATA: VendegloBeszuras' });
  }).then(await insertVendeglo(req.body)).then(() => {
    res.redirect('/');
  })
    .catch((err) => {
      console.error(err);
      res.type('.html').status(400).render('error', { message: 'error WITH THE DATA: VendegloBeszuras' });
    });
});

//
// A foglalas oldal betoltese
//
router.get('/uj_foglalas', async (req, res) => {
  try {
    if (req.cookies.auth) {
      const vendeglok = await findAllVendeglo();
      const foglalasok = await findFoglalasAsztalok(1, 11);
      console.log(foglalasok);
      const { id } = jwt.verify(req.cookies.auth, secret);
      const { name } = jwt.verify(req.cookies.auth, secret);
      const felhasznalo = { id, name };
      res.render('foglalas', { vendeglok, felhasznalo, foglalasok });
    } else {
      res.render('login');
    }
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'Nem sikerult uj foglalast betenni' });
  }
});

//
// A foglalas oldal betoltese
//
router.get('/sajat_foglalasok', async (req, res) => {
  try {
    console.log(jwt.verify(req.cookies.auth, secret).role === 'admin', jwt.verify(req.cookies.auth, secret).role);
    if (jwt.verify(req.cookies.auth, secret).role === 'admin') {
      const foglalasok = await findAllFoglalasAdmin();
      res.render('foglalasok_listazasa', {
        foglalasok,
      });
    } else {
      const foglalasok = await findAllFoglalasUser(jwt.verify(req.cookies.auth, secret).id);
      res.render('foglalasok_listazasa', {
        foglalasok,
      });
    }
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'Nem sikerult betolteni a FOGLALASOK oldalt!' });
  }
});

//
// form_foglalas
//
router.post('/form_foglalas', async (req, res) => {
  try {
    const vid = (req.body.vendeg);
    const fid = (req.body.felhasznalo_n);
    const aid = (req.body.rad);
    const time = (req.body.form_kliens_idopont);
    const nyitvatartas = await getIdopontok(vid);

    if (!time.match(regex) || !vid.match(regex)) {
      console.log('NEM EGY SZAM');
      res.end('NEM EGY SZAM');
      res.type('.html').status(400).render('error', { message: 'NEM EGY SZAM' });
    } else if (time < nyitvatartas.Nyitas || time > nyitvatartas.Zaras) {
      console.log('Ebben az idopontban nincsen nyitva a Vendeglo');
      res.type('.html').status(400).render('error', { message: 'Ebben az idopontban nincsen nyitva a Vendeglo' });
    } else {
      const respBody = `Foglalas érkezett:\n
        Vendeglo ID: ${vid}\n
        Foglalas erre az IDra: ${fid}\n
        Foglalas asztal: ${aid}\n
        Idopont: ${time}\n
      `;

      insertFoglalas(aid, vid, fid, time);

      console.log(respBody);
      res.type('.html').status(200).render('succes', { message: respBody });
    }
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'Nem sikerult uj foglalast betenni' });
  }
});

//
// ajax vendeglo tobb info betoltesehez
//
router.get('/getMoreInfoVendeglo/:id', async (req, res) => {
  try {
    const idVendeglo = req.params.id;
    const vendegloInfo = await findVendeglo(idVendeglo);

    // ha nem letezik a vendeglo kiirom egy uj oldalon
    if (!vendegloInfo) {
      res.type('.html').status(400).render('error', { message: 'NEM SIKERULT LEKERNI A VENDEGLOT' });
      res.end();
      return;
    }
    res.send(JSON.stringify(vendegloInfo[0]));
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'NEM SIKERULT LEKERNI A VENDEGLOT' });
  }
});

//
// ajax foglalasok
//
router.get('/foglaltAsztal/:ido/:vid', async (req, res) => {
  try {
    const idopont = req.params.ido;
    const vendegloID = req.params.vid;

    const foglalInfo = await findFoglalasAsztalok(vendegloID, idopont);
    console.log('Asztal: ', foglalInfo);
    res.send(JSON.stringify(foglalInfo));
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'NEM SIKERULT LEKERNI A FOGLALSOKAT' });
  }
});

//
// ajax nyitva vagy sem
//
router.get('/nyitvaVendeglo/:ido/:vid', async (req, res) => {
  try {
    const idopont = req.params.ido;
    const vendegloID = req.params.vid;
    const v = await findVendeglo(vendegloID);

    if (v[0].Nyitas <= idopont && v[0].Zaras >= idopont) {
      res.send(JSON.stringify('true'));
    } else {
      res.send(JSON.stringify('false'));
    }
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'NEM SIKERULT LEKERNI A FOGLALSOKAT' });
  }
});

//
// Itt adom tovabb az id-t es renderelem be a kepfeltoltes oldalt
//
router.get('/vendeglo/:id', async (req, res) => {
  try {
    const idVendeglo = req.params.id;
    const vendeglok = await findVendeglo(idVendeglo);
    // ha nem letezik a vendeglo kiirom egy uj oldalon
    if (!vendeglok) {
      console.log('NEM SIKERULT LEKERNI A VENDEGLOT');
      res.type('.html').status(400).render('error', { message: 'NEM SIKERULT LEKERNI A VENDEGLOT' });
      res.end();
      return;
    }
    const foglalasok = await findFoglalasok(idVendeglo);
    const kepek = await findKep(idVendeglo);
    let user = 0;
    if (req.cookies.auth) {
      user = jwt.verify(req.cookies.auth, secret).id;
    } else { user = -1; }
    res.render('kepfeltoltes', {
      vendeglok: vendeglok[0], id: idVendeglo, foglalasok, kepek, user,
    });
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'Nem sikerult betolteni a Tovabbi reszletek oldalt!' });
  }
});

//
// Search a fooldalon
//
router.post('/keres', async (req, res) => {
  try {
    const name  = req.body.keres_name;
    const nyitas  = req.body.keres_nyitas;
    const zaras  = req.body.keres_zaras;
    const helyseg  = req.body.keres_helyseg;
    console.log(name);
    const vendeglok = await findVendegloByName(name, helyseg, nyitas, zaras);
    console.log(vendeglok);
    res.render('index', { vendeglok });
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'NEM SIKERULT BETOLTENI A VENDEGLOKET' });
  }
});

//
// Search a foglalasokban
//
router.post('/keres_foglalas', async (req, res) => {
  try {
    // eslint-disable-next-line
    const id = jwt.verify(req.cookies.auth, secret).id;
    const name  = req.body.keres_name;
    const idopont  = req.body.keres_idopont;
    const status  = req.body.keres_status;
    const foglalasok = await findFoglalasFiltered(id, name, status, idopont);
    console.log(foglalasok);
    res.render('foglalasok_listazasa', { foglalasok });
  } catch (err) {
    console.error(err);
    res.type('.html').status(400).render('error', { message: 'NEM SIKERULT BETOLTENI A VENDEGLOKET' });
  }
});

//
// ajax foglalas torlese
//
router.delete('/deleteFoglalas/:FoglalasID', async (req, res) => {
  try {
    const idFoglalas = req.param('FoglalasID');
    await deleteFoglalas(idFoglalas);

    res.status(204);
    res.send();
  } catch (err) {
    console.error(err);
    res.status(500);
    res.send('error');
  }
});

//
// ajax foglalas elfogadasa
//
router.patch('/acceptFoglalas/:FoglalasID', async (req, res) => {
  try {
    const idFoglalas = req.param('FoglalasID');
    await acceptFoglalas(idFoglalas);

    res.status(204);
    res.send();
  } catch (err) {
    console.error(err);
    res.status(500);
    res.send('error');
  }
});

//
// ajax foglalas elutasitasa
//
router.patch('/rejectFoglalas/:FoglalasID', async (req, res) => {
  try {
    const idFoglalas = req.param('FoglalasID');
    await rejectFoglalas(idFoglalas);

    res.status(204);
    res.send();
  } catch (err) {
    console.error(err);
    res.status(500);
    res.send('error');
  }
});

//
// form_kepek
//
const uploadDir = p.join(process.cwd(), 'static/uploadDir');
router.use(express.static(p.join(process.cwd(), 'static')));
router.use(eformidable({ uploadDir }));

router.post('/form_kepek', async (req, res) => {
  const fileHandler = req.files.kliens_file;
  const vid = (req.fields.form_kep_id);

  try {
    if (!fileHandler) {
      console.log('Nem adtal meg kepet!');
      res.type('.html').status(400).render('error', { message: 'Nem adtal meg kepet!' });
    } else {
      const fileNameLast = (fileHandler.path).split(p.sep).pop();

      const respBody = `
        Feltöltés érkezett:
        Vendeglo ID: ${vid}
        állománynév: ${fileHandler.name}
        név a szerveren: ${fileNameLast}
      `;
      await insertKep(vid, fileNameLast, fileHandler.name);

      console.log(respBody);

      res.type('.html').status(200).render('succes', { message: respBody });
    }
  } catch (err) {
    res.type('.html').status(400).render('error', { message: 'NEM SIKERULT A KEP BESZURASA' });
  }
});

export default router;
