import jwt from 'jsonwebtoken';
import { secret } from '../secret.js';

export function checkJWT(req, res, next) {
  if (!req.cookies.auth) {
    res.locals.jwtToken = null;
  } else {
    res.locals.jwtToken = req.cookies.auth;
  }
  next();
}

export function validateJWT(req, res, next) {
  if (res.locals.jwtToken) {
    const decode = jwt.verify(res.locals.jwtToken, secret);
    res.locals.name = decode.name;
    res.locals.id = decode.id;
    res.locals.role = decode.role;
  }
  next();
}
